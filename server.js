//npm modules
var express = require("express");
var app = express();

app.use(express.static(__dirname + "/public"));

//---- set the view engine to ejs
app.set('view engine', 'ejs');

app.get("/", function (req, res) {
  res.render("");
});

app.listen(8000);
console.log("8000 is Running...");
